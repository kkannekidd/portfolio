# **WELCOME.** #

This is my first portfolio website. 

* Last modified: November 20th, 2016.
* URL: http://kiddography.com/


## **SOURCES** ##

### CSS: ###
* [Bootstrap](http://getbootstrap.com/) - 3.3.6
* [Animate.css](https://daneden.github.io/animate.css/)
* [Hover.css](http://ianlunn.github.io/Hover/) - v2

### JavaScript: ###
* [JQuery](https://jquery.com/) - 2.2.3
* [Chart.js](http://www.chartjs.org/) 
* [Isotope](http://isotope.metafizzy.co/) - v1.5.25
* [JQuery Waypoints](http://imakewebthings.com/waypoints/) - v2.0.3
* [Smooth Scrolling](https://css-tricks.com/snippets/jquery/smooth-scrolling/) - CSS-Tricks
* [Back to Top](http://www.codexworld.com/back-to-top-button-using-jquery-css/) - CodexWorld

## **PENDING** ##

* Media queries/breakpoints for tablets.
* Improve responsive images.
